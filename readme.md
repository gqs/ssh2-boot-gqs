### eclipse或者sts

本项目如果使用eclipse或者sts (Spring Tool Suite),你会发现 src/main/webapp/assets/js/data-time/moment.min.js 这个文件总是报错。

这个报错不是表示这个文件真的有错误，而且这个并不会影响项目的运行，但是这个却会影响到心情，看这总是很不爽，解决办法是：

打开项目的 .project文件可以看到：
	
	<buildSpec>
		<buildCommand>
			<name>org.eclipse.wst.jsdt.core.javascriptValidator</name>
			<arguments>
			</arguments>
		</buildCommand>
		...
	</buildSpec>
	<natures>
		...
		<nature>org.eclipse.wst.jsdt.core.jsNature</nature>
	</natures>

将下面的两个xml节点删除掉,然后重启eclipse即可

	<buildCommand>
			<name>org.eclipse.wst.jsdt.core.javascriptValidator</name>
			<arguments>
			</arguments>
	</buildCommand>
	
	<nature>org.eclipse.wst.jsdt.core.jsNature</nature>