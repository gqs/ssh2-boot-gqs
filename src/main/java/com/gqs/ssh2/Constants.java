package com.gqs.ssh2;

/**
 * 系统中使用的常量
 * @author wuqing
 *
 */
public interface Constants {
	/**
	 * 当前用户，放入到session中使用这个常量作为key
	 */
	public static final String CURRENT_USER="CURRENT_USER";
}
