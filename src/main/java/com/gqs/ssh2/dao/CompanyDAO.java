package com.gqs.ssh2.dao;

import java.util.List;

import com.gqs.ssh2.po.Company;
import com.gqs.ssh2.vo.Page;
import com.gqs.ssh2.vo.PageRequest;

/**
 *@Package com.gqs.ssh2.dao
 *@Class com.gqs.ssh2.dao.CompanyDAO
 *@Description TODO
 *@author gqs
 *@date 2015年4月22日 下午7:55:49
 *@version V1.0
 *Copyright 葛葛梦工厂  Corporation 2015
 */
public interface CompanyDAO {
	//public List<Company> findAll();//全查
	public Page<Company> findByPageRequest(PageRequest pageRequest);
}
