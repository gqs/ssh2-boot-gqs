package com.gqs.ssh2.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gqs.ssh2.dao.BaseDAO;
import com.gqs.ssh2.dao.UserDAO;
import com.gqs.ssh2.po.User;

@Repository
//真正干活的是DAO
public class UserDAOImpl extends BaseDAO implements UserDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findAll() {
		String hql="from User";//User是类名不是表名
		return this.getHibernateTemplate().find(hql);
	}

}
