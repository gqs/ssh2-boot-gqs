package com.gqs.ssh2.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "gqs_user", uniqueConstraints = @UniqueConstraint(columnNames = "account"))
public class User extends BaseEntity implements Serializable {
	
	@Column(name = "account", unique = true, nullable = false, length = 50)
	private String account;
	
	@Column(name = "password", nullable = false, length = 50)
	private String password;
	
	@Column
	private String name;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
