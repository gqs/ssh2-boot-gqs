package com.gqs.ssh2.service;

import java.util.List;

import com.gqs.ssh2.po.Company;
import com.gqs.ssh2.vo.Page;

/**
 *@Package com.gqs.ssh2.service
 *@Class com.gqs.ssh2.service.CompanyService
 *@Description TODO
 *@author gqs
 *@date 2015年4月22日 下午8:09:15
 *@version V1.0
 *Copyright 葛葛梦工厂  Corporation 2015
 */
public interface CompanyService {
	//public List<Company> findAll();
	public Page<Company> findPage(int index, int size);
}
