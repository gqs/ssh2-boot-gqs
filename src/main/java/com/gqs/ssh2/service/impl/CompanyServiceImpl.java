package com.gqs.ssh2.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gqs.ssh2.dao.CompanyDAO;
import com.gqs.ssh2.po.Company;
import com.gqs.ssh2.service.CompanyService;
import com.gqs.ssh2.vo.Page;
import com.gqs.ssh2.vo.PageRequest;

/**
 *@Package com.gqs.ssh2.service.impl
 *@Class com.gqs.ssh2.service.impl.CompanyServiceImpl
 *@Description TODO
 *@author gqs
 *@date 2015年4月22日 下午8:09:57
 *@version V1.0
 *Copyright 葛葛梦工厂  Corporation 2015
 */
@Service
public class CompanyServiceImpl implements CompanyService {
	@Autowired
	private CompanyDAO companyDAO;
	/* (non-Javadoc)
	 * @see com.gqs.ssh2.service.CompanyService#findAll()
	 */
//	@Override
//	public List<Company> findAll() {
//		return companyDAO.findAll();
//	}
	/* (non-Javadoc)
	 * @see com.gqs.ssh2.service.CompanyService#findPage(int, int)
	 */
	@Override
	public Page<Company> findPage(int index, int size) {
		PageRequest pageRequest = new PageRequest(index, size);
		return companyDAO.findByPageRequest(pageRequest);
	}

}
