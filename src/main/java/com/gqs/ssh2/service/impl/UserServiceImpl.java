package com.gqs.ssh2.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gqs.ssh2.po.User;
import com.gqs.ssh2.service.UserService;
import com.gqs.ssh2.dao.UserDAO;

@Service
//此步骤加@Service的作用是使spring容器自动扫描到该包并且将对应的实现类对象实例化
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDAO userDao;

	@Override
	public List<User> find(String name) {//这里假设全查，里面实际返回全查
		return userDao.findAll();
	}
}
