package com.gqs.ssh2.vo;

import java.util.Map;

/**
 *@Package com.gqs.ssh2.vo
 *@Class com.gqs.ssh2.vo.PageRequest
 *@Description TODO
 *@author gqs
 *@date 2015年4月23日 上午8:49:01
 *@version V1.0
 *Copyright 葛葛梦工厂  Corporation 2015
 */
public class PageRequest {
	private Integer index;
	private Integer size;
	private Map<String, String> condition;
	
	public Integer getFirstResult(){
		return (index-1)*size;
	}
	
	public PageRequest(Integer index, Integer size) {
		super();
		this.index = index;
		this.size = size;
	}
	
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public Map<String, String> getCondition() {
		return condition;
	}
	public void setCondition(Map<String, String> condition) {
		this.condition = condition;
	}
	
}