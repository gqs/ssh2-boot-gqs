package com.gqs.ssh2.web.action.home;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;

@Controller
//这里虽然继承了struts.xml，但是访问action时并不以里面的namespace="/gqs"这个为访问空间，而是以约定为action的访问空间
@ParentPackage(value = "ssh2-default")
public class HomeAction extends ActionSupport {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3480002956836990332L;

	@Action("index")
	public String index(){
		return SUCCESS;
	}
}
