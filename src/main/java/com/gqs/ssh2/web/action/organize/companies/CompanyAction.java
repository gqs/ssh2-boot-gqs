package com.gqs.ssh2.web.action.organize.companies;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.gqs.ssh2.po.Company;
import com.gqs.ssh2.service.CompanyService;
import com.gqs.ssh2.vo.Page;
import com.opensymphony.xwork2.ActionSupport;

@Controller
@ParentPackage(value = "ssh2-default")
public class CompanyAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private CompanyService companyService;
	
//	private List<Company> companyList;
//	
//	public List<Company> getCompanyList() {//页面从这个方法获取到值
//		return companyList;
//	}
//
//	@Action("list")
//	public String list(){
//		companyList = companyService.findAll();
//		return SUCCESS;
//	}
	
	private int index=1;//????
	private int size=5;
	
	public void setIndex(int index) {
		this.index = index;
	}
	public void setSize(int size) {
		this.size = size;
	}
	
	private Page<Company> page;

	public Page<Company> getPage() {//从页面获取到
		return page;
	}
	
	@Action("list")
	public String list(){
		page= companyService.findPage(index, size);
		return SUCCESS;
	}
}
