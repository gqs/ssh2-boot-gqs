package com.gqs.ssh2.web.action.organize.departments;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;

@Controller
@ParentPackage(value = "ssh2-default")
public class DepartmentAction extends ActionSupport {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Action("list")
	public String list(){
		return SUCCESS;
	}
}
