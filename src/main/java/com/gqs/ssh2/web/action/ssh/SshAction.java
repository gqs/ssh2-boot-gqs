package com.gqs.ssh2.web.action.ssh;
/**
 * 写这个类的目的是当用户输入：127.0.0.1:8080/ssh/时，页面不会报错，而是由这里进入到index.jsp页面
 */
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;

/**
 *@Package com.gqs.ssh2.action
 *@Class com.gqs.ssh2.action.IndexAction
 *@Description TODO
 *@author gqs
 *@date 2015年4月29日 下午7:23:25
 *@version V1.0
 *Copyright 葛葛梦工厂  Corporation 2015
 */
@Controller
/**
 * 这里先是继承了默认的action映射文件，即要用它的namespace="/gqs"的命名空间下再进入约定的action访问路径
 * 而后又自定义"/"，所以要摒弃上面，用"/"代替
 */

@Namespace("/")
public class SshAction extends ActionSupport{
	
	@Action(value="/",results={@Result(name=SUCCESS,type="redirect",location="users/login.action")
	})
	public String ssh(){
		return SUCCESS;
	}

}
