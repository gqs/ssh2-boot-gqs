package com.gqs.ssh2.web.action.users;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.dispatcher.SessionMap;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
/**
 * 
 * 使用struts2 默认的package，即默认从convention-default继承而来，
 * 而convention-default这个package中没有定义auth(com.gqs.ssh2.web.interceptor.AuthenticationInterceptor)拦截器
 * @author 
 *
 */
@Controller
//底下访问的这个时要用/路径[约定大于配置]
@Namespace("/")
public class SessionAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 常量，放入到session中使用这个常量作为key
	 */
	public static final String CURRENT_USER="CURRENT_USER";
	/**
	 * 展示登录页面
	 * @return
	 */
	@Action(value="login",results={@Result(name=SUCCESS,location="home/login.jsp")
	})
	public String login(){
		return SUCCESS;
	}
	
	/**
	 * 登录认证
	 * @return
	 */
	@Action(value="authen",results={@Result(name=SUCCESS,type="redirect",location="home/index.action")
	})
	public String authenticate(){
		//TODO 完成认证逻辑
		ActionContext.getContext().getSession().put(CURRENT_USER, "张三");
		return SUCCESS;
	}
	
	/**
	 * 退出登录
	 * @return
	 */
	@Action(value="loginout",results={@Result(name=SUCCESS,type="redirectAction",location="login")
	})
	public String signout(){
		//使session失效
		SessionMap<String,Object> session=(SessionMap<String,Object>)ActionContext.getContext().getSession();
		session.invalidate();
		//ServletActionContext.getRequest().getSession().invalidate(); 也可以使用这种方法
		return SUCCESS;
	}
}
