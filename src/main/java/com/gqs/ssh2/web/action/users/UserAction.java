package com.gqs.ssh2.web.action.users;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.gqs.ssh2.po.User;
import com.gqs.ssh2.service.UserService;
import com.opensymphony.xwork2.ActionSupport;

@Controller
@ParentPackage(value = "ssh2-default")
public class UserAction extends ActionSupport {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private UserService userService;
	
	private List<User> users;

	public List<User> getUsers() {//页面从这个获取到值
		return users;
	}


	@Action("list")
	public String list(){
		users=userService.find(null);
		return SUCCESS;
	}
}
