package com.gqs.ssh2.web.interceptor;


import com.gqs.ssh2.Constants;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

/**
 * 用户身份认证，即是否登录成功的判断
 * @author gqs
 *
 */
public class AuthenticationInterceptor extends MethodFilterInterceptor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected String doIntercept(ActionInvocation invocation) throws Exception {
		String currentUser=(String) ActionContext.getContext().getSession().get(Constants.CURRENT_USER);
		if(currentUser==null){
			//一旦认证失败后，需要给用户提示信息
			//而返回的视图是 redirect过去的，如果放在request中，那么提示信息无法带入到登录页面，所以这里放入到session中
			//页面中使用 <s:property value="#session.remove('not_login')"/>标签来取出提示信息。
			//它的作用就是调用了session（SessionMap）类中的remove方法，将提示消息取出来，然后删除掉它
			//注：OGNL表达式中是可以调用对象的方法的。
			ActionContext.getContext().getSession().put("not_login", "您需要登录后再访问!");
			return "login";
		}
		return invocation.invoke();
	}

}
