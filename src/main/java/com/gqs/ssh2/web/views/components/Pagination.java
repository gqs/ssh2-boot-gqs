package com.gqs.ssh2.web.views.components;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.UIBean;
import org.apache.struts2.dispatcher.mapper.ActionMapping;
import org.apache.struts2.views.annotations.StrutsTagAttribute;

import com.gqs.ssh2.vo.Page;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;

/**
 *@Package com.gqs.ssh2.web.views.components
 *@Class com.gqs.ssh2.web.views.components.Pagination
 *@Description 扩展strtus2的标签库，实现分页显示的标签
 *@author gqs
 *@date 2015年4月26日 上午10:16:10
 *@version V1.0
 *Copyright 葛葛梦工厂  Corporation 2015
 */
public class Pagination extends UIBean {
	final public static String TEMPLATE = "pagination";//pagination页码
	protected String pageBean;

	@StrutsTagAttribute(description="获取分页对象com.gqs.ssh2.vo.Page的OGNL表达式", 
			type="com.gqs.ssh2.vo.Page")
	public void setPageBean(String pageBean) {
		this.pageBean = pageBean;
	}

	/**
	 * @param stack
	 * @param request
	 * @param response
	 */

	public Pagination(ValueStack stack, HttpServletRequest request,
			HttpServletResponse response) {
		super(stack, request, response);
	}

	/* (non-Javadoc)
	 * @see org.apache.struts2.components.UIBean#getDefaultTemplate()
	 */

	protected void evaluateExtraParams() {
		super.evaluateExtraParams();
		if (pageBean != null) {
			//将Page对象放入到FreeMark 模板上下文中，其key为 pageBean
			//findValue 其实就是计算OGNL表达式
			addParameter("pageBean",super.findValue(pageBean, Page.class));
			ActionMapping actionMapping= (ActionMapping) super.request.getAttribute("struts.actionMapping");
			
			String requestUri=super.findString("%{#attr['struts.request_uri']}");
			//放入actionMapping，是为了获取到namespace与action
			addParameter("actionMapping",actionMapping);
			addParameter("requestUri",requestUri);
		}
	}
	
	
	@Override
	protected String getDefaultTemplate() {
		return TEMPLATE;
	}

}
