package com.gqs.ssh2.web.views.jsp.tags;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ui.AbstractUITag;

import com.gqs.ssh2.web.views.components.Pagination;
import com.opensymphony.xwork2.util.ValueStack;

/**
 *@Package com.gqs.ssh2.web.views.jsp.tags
 *@Class com.gqs.ssh2.web.views.jsp.tags.PaginationTag
 *@Description 分页标签
 *@author gqs
 *@date 2015年4月26日 上午10:18:33
 *@version V1.0
 *Copyright 葛葛梦工厂  Corporation 2015
 */
public class PaginationTag extends AbstractUITag {
	private String pageBean;
	

	/* (non-Javadoc)
	 * @see org.apache.struts2.views.jsp.ComponentTagSupport#getBean(com.opensymphony.xwork2.util.ValueStack, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public Component getBean(ValueStack stack, HttpServletRequest req,
			HttpServletResponse res) {
		return new Pagination(stack,req,res);
	}

	public void setPageBean(String pageBean) {
		this.pageBean = pageBean;
		this.setTheme("bootstrap");
	}
	
	protected void populateParams() {
		
        super.populateParams();
        Pagination pg = ((Pagination) component);
        pg.setPageBean(pageBean);
    }
}
