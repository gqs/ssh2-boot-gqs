/****************************用户**************************/
insert into gqs_user(`account`,`password`,`name`) values ('zhangsan@126.com','zhangsan','张三');
insert into gqs_user(`account`,`password`,`name`) values ('lisi@126.com','lisi','李四');
insert into gqs_user(`account`,`password`,`name`) values ('wangwu@126.com','wangwu','王五');
insert into gqs_user(`account`,`password`,`name`) values ('zhaoliu@126.com','zhaoliu','赵六');


/****************************分公司**************************/
insert into company(`name`,`address`,`intro`,`principal`,`principalTel`,`tel`,`parentCompanyId`) values ('金智云起点总部','西安市高新区科技二路清华科技园','金智java软件工程师培训','李鹏','18991910998','18991910998',null);
insert into company(`name`,`address`,`intro`,`principal`,`principalTel`,`tel`,`parentCompanyId`) values ('金智云起点第一分公司','XXXXXX','金智java软件工程师培训','刘洪旺','18991910998','18991910998',1);
insert into company(`name`,`address`,`intro`,`principal`,`principalTel`,`tel`,`parentCompanyId`) values ('金智云起点第二分公司','XXXXXX','金智java软件工程师培训','刘洪旺','18991910998','18991910998',1);
insert into company(`name`,`address`,`intro`,`principal`,`principalTel`,`tel`,`parentCompanyId`) values ('金智云起点第三分公司','XXXXXX','金智java软件工程师培训','刘洪旺','18991910998','18991910998',1);
insert into company(`name`,`address`,`intro`,`principal`,`principalTel`,`tel`,`parentCompanyId`) values ('金智云起点第四分公司','XXXXXX','金智java软件工程师培训','刘洪旺','18991910998','18991910998',1);
insert into company(`name`,`address`,`intro`,`principal`,`principalTel`,`tel`,`parentCompanyId`) values ('金智云起点第五分公司','XXXXXX','金智java软件工程师培训','刘洪旺','18991910998','18991910998',1);
insert into company(`name`,`address`,`intro`,`principal`,`principalTel`,`tel`,`parentCompanyId`) values ('金智云起点第六分公司','XXXXXX','金智java软件工程师培训','刘洪旺','18991910998','18991910998',1);
insert into company(`name`,`address`,`intro`,`principal`,`principalTel`,`tel`,`parentCompanyId`) values ('金智云起点第七分公司','XXXXXX','金智java软件工程师培训','刘洪旺','18991910998','18991910998',1);
insert into company(`name`,`address`,`intro`,`principal`,`principalTel`,`tel`,`parentCompanyId`) values ('金智云起点第八分公司','XXXXXX','金智java软件工程师培训','刘洪旺','18991910998','18991910998',1);
insert into company(`name`,`address`,`intro`,`principal`,`principalTel`,`tel`,`parentCompanyId`) values ('金智云起点第九分公司','XXXXXX','金智java软件工程师培训','刘洪旺','18991910998','18991910998',1);
insert into company(`name`,`address`,`intro`,`principal`,`principalTel`,`tel`,`parentCompanyId`) values ('金智云起点第十分公司','XXXXXX','金智java软件工程师培训','刘洪旺','18991910998','18991910998',1);
insert into company(`name`,`address`,`intro`,`principal`,`principalTel`,`tel`,`parentCompanyId`) values ('金智云起点第十一分公司','XXXXXX','金智java软件工程师培训','刘洪旺','18991910998','18991910998',1);
insert into company(`name`,`address`,`intro`,`principal`,`principalTel`,`tel`,`parentCompanyId`) values ('金智云起点第十二分公司','XXXXXX','金智java软件工程师培训','刘洪旺','18991910998','18991910998',1);
insert into company(`name`,`address`,`intro`,`principal`,`principalTel`,`tel`,`parentCompanyId`) values ('金智云起点第十三分公司','XXXXXX','金智java软件工程师培训','刘洪旺','18991910998','18991910998',1);