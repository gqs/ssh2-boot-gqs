<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 重定向不能直接访问web-inf里的文件，所以这里使用action -->
<c:redirect url="/home/index.action" />