package com.gqs.ssh2.test.company;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.gqs.ssh2.test.BaseTests;
import com.gqs.ssh2.po.Company;
import com.gqs.ssh2.service.CompanyService;
import com.gqs.ssh2.vo.Page;

/**
 *@Package com.gqs.ssh2.test.company
 *@Class com.gqs.ssh2.test.company.CompanyServiceTests
 *@Description TODO
 *@author gqs
 *@date 2015年4月23日 下午3:04:14
 *@version V1.0
 *Copyright 葛葛梦工厂  Corporation 2015
 */
public class CompanyServiceTests extends BaseTests {
	
	@Autowired
	private CompanyService companyService;
	
	@Test
	public void testFindPage(){
		int index=1,size=5;//分页查询索引这块还不不太明白
		Page<Company> page=companyService.findPage(index, size);
		
		Assert.assertEquals(Long.valueOf(14), page.getTotal());
		Assert.assertEquals(Integer.valueOf(3), page.getTotalPage());
		Assert.assertEquals(true, page.isFirst());
		Assert.assertEquals(false, page.isLast());
		Assert.assertEquals(true, page.hashData());
		Assert.assertEquals(true, page.hashNext());
		Assert.assertEquals(false, page.hashPrevious());
		Assert.assertEquals(Long.valueOf(1), page.getData().get(0).getId());//?
	}
}