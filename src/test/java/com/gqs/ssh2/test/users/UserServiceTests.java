package com.gqs.ssh2.test.users;

import java.util.HashMap;
import java.util.Map;

import org.apache.struts2.views.util.DefaultUrlHelper;
import org.apache.struts2.views.util.UrlHelper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.gqs.ssh2.test.BaseTests;
import com.gqs.ssh2.service.UserService;

public class UserServiceTests extends BaseTests {
	@Autowired
	private UserService service;
	@Test
	public void testFindAll(){
		Assert.assertEquals(4,service.find(null).size());
	}
	@Test//??
	public void testUrlHelper(){
		UrlHelper helper=new DefaultUrlHelper();
		StringBuilder link=new StringBuilder("http://127.0.0.1/test");
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("a", "参数A");
		params.put("b", "参数b");
		params.put("c", "参数c");
		helper.buildParametersString(params, link, "&");
		System.out.println(link.toString());
	}
}
